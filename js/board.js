// NOTE: this code uses the chess.js library:
// https://github.com/jhlywa/chess.js

'use strict';

var ChessBoard = function (boardID, params) {

    // Use the same variable name 'boardID' for the variable on which the ChessBoard is assigned.
    // (This is to handle promotions. FIXME: Find a better way of doing it.)

    var board = null;
    var game = new Chess();

    var selectedPiece = null;
    var promotionPiece = null;
    var sparingPiece = null;
    var validTargets = null;

    var piecesTheme = 'staunty';
    var piecesFormat = 'svg';

    var previousFEN;
    var gameMoves = [];
    var undoneMoves = [];

    const INITIAL_FEN = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
    const piecesPath = 'board/img/chesspieces/';
    const move_sound = new Audio("board/audio/Move.mp3");
    const capture_sound = new Audio("board/audio/Capture.mp3");
    const trash_sound = new Audio("board/audio/Trash.mp3");


    function highlightSensitive(square) {
        var squareEl = $('#' + boardID + ' .square-' + square);
        var hcolor;
        if (squareEl.hasClass('black-3c85d') === true) { hcolor = 'highlight-candidate-dark'; }
        else { hcolor = 'highlight-candidate-light'; }
        squareEl.addClass(hcolor);
    };

    function candidateSquare(square) {
        var squareEl = $('#' + boardID + ' .square-' + square);
        var hcolor;
        if (squareEl.hasClass('black-3c85d') === true) { hcolor = 'highlight-dark'; }
        else { hcolor = 'highlight-light'; }
        squareEl.addClass(hcolor);
    };

    function findKing(color) {
        for (let i = 0; i < 64; i++) {
            let piece = game.get(game.SQUARES[i]);
            if (piece && piece.type === 'k' && piece.color == color)
                return game.SQUARES[i];
        }
    }

    function addHighlights(square) {
        var moves = game.moves({
            square: square,
            verbose: true
        });

        // exit if there are no moves available for this square
        if (moves.length === 0) return;

        // highlight the possible squares for this piece
        for (var i = 0; i < moves.length; i++) {
            candidateSquare(moves[i].to);
        }
        $('#' + boardID + ' .square-' + square).addClass('highlight-candidate');
    };

    function removeHighlights() {
        $('#' + boardID + ' .square-55d63').removeClass('highlight-candidate-light');
        $('#' + boardID + ' .square-55d63').removeClass('highlight-candidate-dark');
        $('#' + boardID + ' .square-55d63').removeClass('highlight-candidate');

        // Remove highlighted checks:
        $('#' + boardID + ' .white-1e1d7').removeClass('red-light');
        $('#' + boardID + ' .black-3c85d').removeClass('red-dark');

        $('#' + boardID + ' .white-1e1d7').removeClass('grey-light');
        $('#' + boardID + ' .black-3c85d').removeClass('grey-dark');
    };

    function ephemeralHighlight(square, hcolor, ms) {
        var squareEl = $('#' + boardID + ' .square-' + square);
        var hclass = hcolor + '-light';
        if (squareEl.hasClass('black-3c85d') === true) { hclass = hcolor + '-dark'; }

        squareEl.addClass(hclass);

        if (ms) {
            setTimeout(function () {
                $('#' + boardID + ' .white-1e1d7').removeClass(hcolor + '-light');
                $('#' + boardID + ' .black-3c85d').removeClass(hcolor + '-dark');
            }, ms);
        }
    };

    function clickOnPromotion(piece) {
        buildBoard(true, game.fen(), false, board.orientation());
        promotionPiece = piece[1].toLowerCase();
    };

    function popSelectPiece(target) {
        selectedPiece = null;
        buildBoard(false, game.fen(), false, board.orientation());
        var piecesStyle = $('#' + boardID).find('.piece-417db')[0].style;
        var col = target[0];
        var rows;
        if (target[1] === '8') { rows = [[8, 'wQ'], [7, 'wR'], [6, 'wB'], [5, 'wN']]; }
        else { rows = [[1, 'bQ'], [2, 'bR'], [3, 'bB'], [4, 'bN']]; }
        $('#' + boardID).find('.square-55d63').addClass('promotion-mask');
        for (var i = 0; i < rows.length; i++) {
            var thisSquare = col + rows[i][0];
            var el = document.getElementById(boardID).getElementsByClassName('square-' + thisSquare)[0];
            var img = '<img id="prom' + i + '" src="' + piecesPath + piecesTheme + '/' + rows[i][1] + '.' + piecesFormat + '"';
            img += 'style="width: ' + piecesStyle.width + '">';
            var link = '<a href="#" onClick="' + boardID + '.clickOnPromotion(\'' + rows[i][1] + '\')" ';
            link += 'style="z-index:100">' + img + '</a>';
            el.innerHTML = link;
            $('#' + boardID).find('.square-' + thisSquare).addClass('promotion-background').removeClass('promotion-mask');
        }
    };

    function askForPromotion(source, target, piece) {
        if (!promotionPiece) {
            setTimeout(function () { askForPromotion(source, target, piece); }, 300);

        } else {
            onDrop(source, target, piece);
            onSnapEnd();
        }
    };

    function onDragStart(source, piece, position, orientation) {
        // do not pick up pieces if the game is over
        if (game.game_over()) return false

        // only pick up pieces for the side to move
        if ((game.turn() === 'w' && piece.search(/^b/) !== -1) ||
            (game.turn() === 'b' && piece.search(/^w/) !== -1)) {
            return false
        }

        validTargets = [];
        var fen = game.fen();
        for (var i = 0; i < game.SQUARES.length; i++) {
            // see if the piece can go to SQUARES[i]
            var move = game.move({ from: source, to: game.SQUARES[i], promotion: 'q' });
            if (move != null) {
                validTargets.push(game.SQUARES[i]);
                game.undo(move);//oad(fen); // game.undo() would spoil enpassant moves if available
            }
        }

        //addHighlights(source);
    };

    function cancelSparing() {
        destroyPhantomPiece();
        sparingPiece = null;
        $('#' + boardID).find('.piece-417db').removeClass('highlight-spare');
    };

    function onDragStartSpare(source, piece) {
        if (source !== 'spare') { return; }
        if (sparingPiece === piece) { cancelSparing(); return; }

        sparingPiece = piece;

        var pieces = $('#' + boardID).find('.piece-417db');

        var id = ['K', 'Q', 'R', 'B', 'N', 'P'].indexOf(piece.substr(1, 1));
        if (piece.substr(0, 1) === board.orientation().substr(0, 1)) {
            id += pieces.length - 6;
        }

        var selectedStyle = 'highlight-spare';
        pieces.removeClass('piece-417db');
        pieces.removeClass(selectedStyle);
        pieces[id].className += " " + selectedStyle;
        pieces.addClass('piece-417db');
    };

    function onDragMove(target, last, source) {
        removeHighlights();

        if (selectedPiece)
            highlightSensitive(source);

        if (validTargets.indexOf(target) >= 0) {
            highlightSensitive(target);
        }
    };

    function onClick(square, piece, position, orientation) {
        removeHighlights();
        if (selectedPiece) {
            onDrop(selectedPiece.loc, square, selectedPiece.piece);
        }
        onSnapEnd();
    };

    function onClickSpare(square, piece) {
        if (piece === true) { return; }
        if (piece === 'substitute') { move_sound.play(); }
        else { move_sound.play(); }

        var phantomPiece = document.getElementById("phantom-piece");
        if (phantomPiece) { phantomPiece.style.opacity = 1; }

        var pos = board.position();
        pos[square] = sparingPiece;
        setTimeout(function () { board.position(pos, false); }, 100);
    };


    function onDrop(source, target, piece) {

        removeHighlights();
        // if source = target, a piece is selected for clicking move
        if (source === target) {
            if (selectedPiece) {
                if (selectedPiece.loc === source) {
                    selectedPiece = null;
                    return;
                }
            }
            selectedPiece = { loc: source, piece: piece };
            highlightSensitive(source);
            //addHighlights(source);
        }

        var fen = game.fen();
        var prom = 'q';
        var waiting = 10;
        var is_capture = false;
        if (promotionPiece) { prom = promotionPiece }
        if (selectedPiece) { waiting = 90; }
        if (game.get(target)) { is_capture = true; }

        // see if the move is legal
        var move = game.move({
            from: source,
            to: target,
            promotion: prom
        });

        // illegal move
        if (move === null) return 'snapback'

        selectedPiece = null;
        previousFEN = fen;

        // handle promotions
        if ((target.substring(1, 2) === '8' && piece === 'wP') ||
            (target.substring(1, 2) === '1' && piece === 'bP')) {

            if (!promotionPiece) {
                setTimeout(function () { popSelectPiece(target); }, waiting);
                game.undo();
                askForPromotion(source, target, piece);
                return;

            } else {
                promotionPiece = null;
            }
        }

        gameMoves.push(move);
        var stored_move = undoneMoves.pop();
        if (move != stored_move)
            undoneMoves = [];

        if (game.in_check())
            ephemeralHighlight(findKing(game.turn()), 'red');

        if (is_capture) { capture_sound.play(); } else { move_sound.play(); }
        params.afterMove()
    };

    function onDropSpare(source, target, piece) {
        if (source !== 'spare') {
            if (target === 'offboard') { trash_sound.play(); }
            else { move_sound.play(); }
        }

        if (source !== 'spare' && source === target) {
            onClickSpare(source, 'substitute');
        }
    };

    function onMouseoverSquare(square, piece) {
        if (!sparingPiece || piece) { return; }

        var el = document.getElementById(boardID).getElementsByClassName('square-' + square)[0];

        var piecesStyle = $('#' + boardID).find('.piece-417db')[0].style;
        var img = '<img id="phantom-piece" src="' + piecesPath + piecesTheme;
        img += '/' + sparingPiece + '.' + piecesFormat + '"';
        img += 'style="opacity: 0.6; width: ' + piecesStyle.width + '">';
        el.innerHTML = img;
    };

    function destroyPhantomPiece() {
        var phantomPiece = document.getElementById("phantom-piece");
        if (phantomPiece) { phantomPiece.remove(); }
    };

    function onMouseoutSquare(square, piece) {
        destroyPhantomPiece();
    };

    function highlightCandidate(square, piece) {
        if (selectedPiece)
            onDragMove(square, square, selectedPiece.loc);
    };

    // update the board position after the piece snap
    // for castling, en passant, pawn promotion
    function onSnapEnd() {
        board.position(game.fen());
    };

    function highlightSquare(square, hcolor, ms) {

        var color = 'highlight-candidate';
        if (hcolor === 0) { color = 'grey' }
        else if (hcolor === 1) { color = 'green' }
        else if (hcolor === 2) { color = 'blue' }
        else if (hcolor === 3) { color = 'purple' }
        else if (hcolor === 4) { color = 'magenta' }
        else if (hcolor === 5) { color = 'red' }

        ephemeralHighlight(square, color, ms);
    };

    function buildBoard(is_draggable, fen, resetGame, orientation) {

        if (params.sparePieces) {
            var config = {
                draggable: true,
                dropOffBoard: 'trash',
                onDragStart: onDragStartSpare,
                onDrop: onDropSpare,
                onMouseoverSquare: onMouseoverSquare,
                onMouseoutSquare: onMouseoutSquare,
                onMouseClickSquare: onClickSpare,
                onChange: params.onChange,
                showNotation: true,
                sparePieces: true,
                pieceTheme: piecesPath + piecesTheme + '/{piece}.' + piecesFormat
            }
            board = Chessboard(boardID, config);
            board.position(fen, false);
            return;
        }

        var config = {
            draggable: is_draggable,
            onDragStart: onDragStart,
            onDragMove: onDragMove,
            onDrop: onDrop,
            onMouseoverSquare: highlightCandidate,
            onMouseoutSquare: highlightCandidate,
            onSnapEnd: onSnapEnd,
            onMouseClickSquare: onClick,
            showNotation: params.showNotation,
            pieceTheme: piecesPath + piecesTheme + '/{piece}.' + piecesFormat
        };
        if (resetGame === undefined) { resetGame = true; }
        if (resetGame) {
            previousFEN = fen;
            game.load(fen);
            gameMoves = [];
            undoneMoves = [];
        }
        board = Chessboard(boardID, config);
        if (orientation != undefined) { board.orientation(orientation); }
        board.position(fen, false);
    };

    if (params == undefined) { params = { fen: INITIAL_FEN }; }
    if (params.fen === undefined) { params.fen = INITIAL_FEN; }
    if (params.turn === undefined) { params.turn = params.fen.split(' ')[1]; }
    if (params.showNotation === undefined) { params.showNotation = true; }
    if (params.afterMove === undefined) { params.afterMove = function () { }; };
    if (params.sparePieces === undefined) { params.sparePieces = false; }

    buildBoard(params.draggable, params.fen);

    return {

        /*********************************************************
        * PUBLIC API
        *********************************************************/

        board: board,
        game: game,

        // Builder functions

        build: function (is_draggable, fen) {
            return buildBoard(is_draggable, fen);
        },

        resize: function () { board.resize() },

        destroy: function () { board.destroy() },


        // Options and configuration

        setPieceTheme: function (theme, format) {
            piecesTheme = theme;
            piecesFormat = format;
            board.destroy();
            buildBoard(params.is_draggable, params.fen, false);
        },

        cancelSparing: function () { cancelSparing },

        flip: function () { board.flip() },

        orientation: function (side) { board.orientation(side); },

        clickOnPromotion: function (piece) { return clickOnPromotion(piece); },


        // Change the board position

        init: function () { game.load(INITIAL_FEN); board.position(INITIAL_FEN, true); },

        position: function (fen, animation) { board.position(fen, animation); },

        load: function (fen, animation) {
            removeHighlights();
            game.load(fen);
            board.position(fen, animation);
            if (game.in_check())
                ephemeralHighlight(findKing(game.turn()), 'red');
        },

        undo: function () {
            var move = gameMoves.pop();
            if (move) {
                game.undo(move);
                board.position(game.fen(), false);
                undoneMoves.push(move);
                removeHighlights();
            }
        },

        redo: function () {
            var fen = game.fen();
            var move = undoneMoves.pop();
            if (game.move(move) === null) return 'snapback'

            previousFEN = fen;
            board.position(game.fen(), false);
            gameMoves.push(move);
        },

        move: function (uci_move) {
            var promotion;
            var is_capture = game.get(uci_move.substr(2, 2));
            if (uci_move.length >= 5) { promotion = uci_move.substr(4, 1); }

            var fen = game.fen();
            var move = game.move({ from: uci_move.substr(0, 2), to: uci_move.substr(2, 2), promotion: promotion });
            if (move === null) return 'snapback'

            previousFEN = fen;
            gameMoves.push(move);

            if (is_capture) { capture_sound.play(); } else { move_sound.play(); };
            board.position(game.fen());
        },


        // Get information about the position

        fen: function () { return board.fen() },

        piece_at: function (square) { return game.get(square); },

        previous_fen: function () { return previousFEN; },

        last_move: function () {
            var lastMove = game.history({ verbose: true }).slice(-1)[0];
            var uciMove = lastMove.from + lastMove.to;
            if (lastMove.promotion)
                uciMove += lastMove.promotion;
            return uciMove;
        },

        // Other functionalities

        highlight_square: function (square, hcolor, ms) { highlightSquare(square, hcolor, ms); },

        highlight_move: function (move, hcolor, ms) {
            highlightSquare(move.substr(0, 2), hcolor, ms);
            highlightSquare(move.substr(2, 2), hcolor, ms);
        },

        highlight_board: function (ms) {
            for (var i = 0; i < game.SQUARES.length; i++)
                highlightSensitive(game.SQUARES[i]);

            setTimeout(removeHighlights, ms);
        },

        highlight_clear: function () {
            removeHighlights();
        }
    }
};
